# Contents

## 🔉AudioBooks🔉

[Audiobook Reviews](https://audiobookreviews.com/)

[Mobilism Audiobooks](https://forum.mobilism.org/viewforum.php?f=124)

[Tokybook](https://tokybook.com/) - Listen Online

[Audiobooks](https://hotaudiobooks.com/) - Listen Online

[xAudiobooks](https://xaudiobooks.com/)

[LearnOutLoud](https://www.learnoutloud.com/Free-Audiobooks)

[Golden Audiobooks](https://goldenaudiobooks.com/)

[LoudLit](http://loudlit.org/)

[Librivox](https://librivox.org/)

[AudioBB](https://audiobb.com/)

[AppAudioBooks](https://appaudiobooks.com/)

[Poetry Audiobooks](https://archive.org/details/audio_bookspoetry)

[Harry Potter Audiobooks](https://hpaudiobooks.club/)

#### Telegram Audio Books Channels and Groups

[Book House International](https://t.me/BOOK_HOUSE_INTERNATIONAL)

[Audiobooks Archive](https://t.me/AudiobooksArchive)

[English Audiobooks](https://t.me/eng_audiobooks)

[MF Mix eBooks](https://t.me/mfmixebooks)

[eBookz](https://t.me/ebookz)

[Audiobook Collection](https://t.me/Audiobooks_Collection)

## Reading sites

General Reading sites, these sites will mainly contain multiple things

[zLibrary](https://z-lib.org/)

[LibGen](https://libgen.fun/)

[Classic Bookshelf](http://www.classicbookshelf.com/library/)

[PageByPage Books](https://www.pagebypagebooks.com/)

[Luminarium](http://www.luminarium.org/)

[Questia](https://www.gale.com/databases/questia)

[Balwin of Library](https://ufdc.ufl.edu/baldwin/all/thumbs)

[Read](http://www.read.gov/books/)

[Classic Literature](https://classic-literature.co.uk/)

[uBooks](https://www.ubooks.pub/)

[The Eye eBooks](https://the-eye.eu/public/Books/)

[eBook Hunter](https://ebook-hunter.org/)

[IRC eBooks](https://www.reddit.com/r/FREEMEDIAHECKYEAH/wiki/storage#wiki_irc_book_sites)

[BookFI](http://en.bookfi.net)

[BookSee](https://en.booksee.org)

[BookRE](http://bookre.org)

[Gutenberg - 60000 older eBooks](https://www.gutenberg.org/)

[Archive.org's book section (28.5 MILLION texts)](https://archive.org/details/texts)



## Manga

[Curated Managa Site Index (Tons of sites)](https://piracy.moe/#)

[MangaHere](http://www.mangahere.cc/)

[Manga4Life](https://manga4life.com/)

[MangaNelo](https://manganelo.com/)

[MangaHub](https://mangahub.io/)

[MangaPark](https://mangapark.net/)

[MangaTown](https://www.mangatown.com/)

[MangaReader](https://www.mangareader.net/)

[MangaPlus](https://mangaplus.shueisha.co.jp/updates)

[MangaOwl](https://mangaowl.net/)

## Comics

[ComicPunch](https://comicpunch.net/)

[XOXOComics](https://xoxocomics.com/)

[View-Comic](https://view-comic.com/)

[Read Comics Online](https://readcomicsonline.ru/)

[ComicExtra](https://www.comicextra.com/)

[ComicOnlineFree](https://comiconlinefree.net/)

[GetComics](https://getcomics.info/)

[Comix-Load](https://comix-load.in/)

[Zip Comic](https://www.zipcomic.com/)

[Comics All](https://comics-all.com/)

[NewComic](https://newcomic.info/)

[BookDL - _Comics and Manga_](http://booksdl.org/comics0/)

[The Eye Comics](http://the-eye.eu/public/Comics/)

## Children's books

[Children's Library](http://en.childrenslibrary.org/)

[MagicKeys](http://www.magickeys.com/books/)

[ByGosh](https://bygosh.com/)

## Educational / Textbooks / Courses / Tutorials

[Math Resource Index](https://free-math-resources.netlify.app)

[Over a thousand educational sites](https://ivypanda.com/blog/1000-open-textbooks-and-learning-resources-for-all-subjects/)

## NewsPapers

[Mobilism Newspapers](https://forum.mobilism.org/viewforum.php?f=123)

[Google Newspapers](https://news.google.com/newspapers)

[Sanet](https://sanet.st/newspapers/)

[ChronclingAmerica](https://chroniclingamerica.loc.gov/newspapers/)

[Newspapers.com](https://newspapers.com/)

[FreeForBook](https://freeforbook.com/)

[ThoughtCo](https://www.thoughtco.com/us-historical-newspapers-online-by-state-1422215)

[LaTimes](https://latimes.newspapers.com/)

[Library Of Congress](https://www.loc.gov/rr/news/oltitles.html)

[Archive.org newspapers](https://archive.org/details/newspapers)

[NewsPaper and Magazines - Telegram](https://t.me/newspapersmagaziness)

## Magazines

[PDFMagazines](https://pdfmagazines.club/)

[DownMagaz](https://downmagaz.net)

[MagazineLib](https://magazinelib.com/)

[WorldMags](https://www.worldmags.net/)

[PDF-MAGAZINES-DOWNLOAD](https://pdf-magazines-download.com/)

[Archive.org's Magazine Rack](https://archive.org/details/magazine_rack)

[PDFGiant](http://pdf-giant.com/)


#### Telegram Magazine Groups

https://t.me/newspapersmagaziness - Magazines

https://t.me/enmagazine - Magazines

https://t.me/Magazines4U - Magazines

[NewsPaper and Magazines](https://t.me/newspapersmagaziness)

## Programming

[O'Reilly](https://www.oreilly.com/)

[For-Coder](https://forcoder.su/)

[The Odin Project](https://www.theodinproject.com/)

[Avax Home](https://www.avaxhome.co/)

[Small HTML Cheat Sheet](https://web.stanford.edu/group/csp/cs21/htmlcheatsheet.pdf)

[Programming Books](https://www.programming-book.com/)

## Articles 

## History

## Subreddits
Subreddits Dedicated to Free eBooks

## Tools
